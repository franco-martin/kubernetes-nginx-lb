# Kubernetes Nginx LB
Deploy a highly available layer 4 loadbalancer using nginx

# Requirements
- (MetalLB)[https://metallb.universe.tf/]

# Architecture

# Installation in microk8s
1. Deploy the yaml files in this repos
2. Get the Loadbalancer IP using `kubectl get svc -n kube-system kubernetes-lb`. It will be under 'EXTERNAL-IP'. For this example we will use 192.168.1.200
3. [Optional] Create a DNS record for your IP address, in this example I will use kubernetes.homelab.lan
4. Refresh the kubernetes certificates on each server one by one by running:
    1. Modify `/var/snap/microk8s/current/certs/csr.conf.template`. Add the following entries under '[ alt_names ]':
        - IP.3 = 192.168.1.200 # Replace the IP.X for whatever entries you have IP.1, IP.2 were used in my case
        - DNS.6 = kubernetes.homelab.lan # Replace the DNS.X for whatever entries you have.
    4. Refresh the certificates with `microk8s refresh-certs --cert front-proxy-client.crt` and `microk8s refresh-certs --cert server.crt`
    5. Validate the configuration by running `echo | openssl s_client -showcerts -connect your_nodes_ip:16443 2>/dev/null | openssl x509 -inform pem -noout -text` and validating the DNS name or IP address are added under "X509v3 Subject Alternative Name"
5. Update your kubeconfig file to point to the DNS or IP address used by MetalLB.
6. Test your connectivity